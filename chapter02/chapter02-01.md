# Programming Fundamental-- Implemented in `Python`: Part 1, Expression, Type, Variable and Program Structure
# 程序设计基础 —— 以 `Python` 实现：第一部分，表达式，类型，变量和程序结构

In the previous chapter, we had a preliminary understanding of `Python`, but it can only be said to be perceptual. Next, let's start with the programming basics and let `Python` work for us.

上一章中，我们对 `Python` 有了初步了解，但也只能说是感性认识。
接下来我们开始从基础学起，让 `Python` 为我们所用。

## Tasks
## 本章任务

- Understand the basic elements and structure of the program
- Demonstration with `Python` package `turtle`
- Complete the excerises

- 了解程序的基本组成元素以及基本结构
- 通过 `turtle` 实现基本的程序结构演示
- 完成课后练习

## 组成程序的基本元素
## The Basic Elements That Make Up the Program

Any program is compose of two parts, one is data structures, and another is control flow.
Both of them are implemented by statments and expressions, the most basic elemments of program.

任何程序都是由两部分组成，一部分是，数据结构，另一部分是，控制流。他们俩又是通过声明式和表达式来实现的，表达式时程序中最基本的元素。

### 声明式
### Statements



### Keywords
### 关键字



```python
import keyword
print(keyword.kwlist)

# The output:
# ['False', 'None', 'True', '__peg_parser__', 'and', 'as', 'assert', 'async', 'await', 'break', 'class', 'continue', 'def', 'del', 'elif', 'else', 'except', 'finally', 'for', 'from', 'global', 'if', 'import', 'in', 'is', 'lambda', 'nonlocal', 'not', 'or', 'pass', 'raise', 'return', 'try', 'while', 'with', 'yield']
```

### Variables and Types
### 变量与类型

> `Python` is a weakly typed language (as opposed to the strongly typed language),
> it is the most important concept in this section, that means you shouldn't to declare
> the type of any variable, and also means that you almost can not tell the type of a variable
> from the source code(in fact, it is not the truth, because after `Python 3.5`, `Python` introduces a type hinting mechanism).

Although `Python` is a weakly typed language, actually type information is stored in each variable.

There are six basic types in `Python`:

> `Python` 是一种弱类型语言（相对于强类型语言来说），记住，这是本小结最重要的概念。弱类型的意思是你不需要为任何变量声明类型，单同事也意味着你几乎不能从源代码层面辨识出变量的类型（事实上并不是这样，因为 `Python 3.5` 之后，`Python` 引入了一种类型提示机制）。

虽然 `Python` 是一种弱类型语言，但实际上每个变量中都储存了类型信息。

`Python` 包含六种基本类型：

- Number（数字）
- String（字符串）
- List（列表）
- Tuple（元组）
- Set（集合）
- Dictionary（字典）

```python
f = 3.14
type(f)

i = 0
type(i)
```

> Output:
> \<class 'float'\>
> \<class 'int'\>

```python
name = "An Ci"
type(name)
```

> Output:
> \<class 'str'\>

```python
alist = [1, 2, 3, "100", "A string"] # or alist = list()
type(alist)
```

> Output:
> \<class 'list'\>

```python
atuple = (1, 2, 3, "100", "A string") # or atuple = tuple()
type(atuple)
```

> Output:
> \<class 'tuple'\>

```python
adict = {'key': 'value'} # or adict = dict()
type(adict)
```

> Output:
> \<class dict\>

```python
aset = {'1', '2'}
type(aset)
```

> Output:
> \<class 'set'\>

### Expression
### 表达式

After statements, let's see what are expressions. Let me introduce some kinds of important expressions accoriding to the syntax of `Python`.

In general, an expression is a set of evaluation operations, that is, an expression must correspond to a result data with its type information.

声明式之后，让我们看看什么是表达式。下面我将根据 `Python` 的语法介绍几类重要的表达式。

一般来说，表达式是一组求值操作，也就是一个表达式一定会对应一个带有类型的结果数据。


## Control Flow Structure
## 控制流结构

There are three main control flow structures in programming:

- Sequential structure
- Branch structure
- Loop structure

It can be said that the rational use and combination of these three structures have created all the programs in the real world.

程序设计中主要有三种控制流结构：

- 顺序结构
- 分支结构
- 循环结构

可以说，这三种结构的合理运用与组合，造就了真实世界中所有的程序。

### Sequential Structure
### 顺序结构

Sequential structure is compose of a set of statements and expressions which are layouted and run as a specific logical sequence, it is simple and intuitive，and it is the most basic programming control structure.

The following code snippet shows a simple programm which draw a circle in a sequential structure:

顺序结构是一组声明式和表达式按照指定的逻辑顺序依次执行的结构，结构简单，表达直观，是最基础的程序控制结构。

下面代码段战士了一个简单的通过顺序结构绘制圆的程序：

```python
import turtle as t

# Basic settings of the pen
t.pensize(4)

# Draw a black ring
t.penup()
t.goto(0, 0)
t.pencolor("black")
t.pendown()
t.circle(50)
```

### Branch Structure
### 分支结构



### Loop Structure
### 循环结构



## Excerises
## 课后练习

#### 1. 绘制奥运五环
#### 1. Draw the Five Olympic Rings

Write a **Sequence Structure** program to draw the five Olympic rings:

编写一个 **顺序结构** 的程序来绘制如下图的奥运五环，请在主体程序基础上添加必要的代码：

![5rings](images/5rings.png)

```python
# Import the required package
import turtle as t

# Basic settings of the pen
t.pensize(4)

# 1. Draw the first ring with blue color
t.penup()
t.goto(100, 100)
t.pencolor("blue")
t.pendown()
t.circle(50)

# Pleawe fill the following required code snippets

# 2. Draw the second ring with black color

# 3. Draw the third ring with red color

# 4. Draw the fourth ring with yellow color

# 5. Draw the fifth ring with green color
```

#### 2. 绘制同切圆
#### 2. Draw Tangent Circles

Write a **loop structure** program to draw the following picture, tangent circles:

编写一个 **循环结构** 的程序绘制如下图的同切圆：

![tangent-circles](images/tangent-circles.png)

```python
# Import the required package
import turtle as t

# Basic settings of the pen
t.pensize(4)
```

#### 3. 绘制颜色相间同心圆
#### 3. Draw Tangent Circles with Two Alternating Colors

Write a program contains **branch structure** to draw tangent circles with two alternating colors, red and black:

编写一个带有 **分支结构** 的程序绘制如下颜色相间的同心圆，红色和黑色：

![tangent-circles-colorful](images/tangent-circles-colorful.png)

```python
# Import the required package
import turtle as t

# Basic settings of the pen
t.pensize(4)

# Draw tangent circles with two alternate colors, red and black
```
