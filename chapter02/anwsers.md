# Answers to Excerises

## Draw the Five Olympic Rings

```python
# Import the required package
import turtle as t

# Basic settings of the pen
t.pensize(4)

# 1. Draw the first ring with blue color
t.penup()
t.goto(100, 100)
t.pencolor("blue")
t.pendown()
t.circle(50)

# Pleawe fill the following required code snippets

# 2. Draw the second ring with black color
t.penup()
t.goto(220, 100)
t.pencolor("black")
t.pendown()
t.circle(50)

# 3. Draw the third ring with red color
t.penup()
t.goto(340, 100)
t.pencolor("red")
t.pendown()
t.circle(50)

# 4. Draw the fourth ring with yellow color
t.penup()
t.goto(160, 50)
t.pencolor("yellow")
t.pendown()
t.circle(50)

# 5. Draw the fifth ring with green color
t.penup()
t.goto(280, 50)
t.pencolor("green")
t.pendown()
t.circle(50)
```

## Draw Tangent Circles

```python
# Import the required package
import turtle as t

# Basic settings of the pen
t.pensize(4)

t.penup()
t.goto(-200,-50)J
t.pendown()
t.pencolor("black")
for radius in [10, 20, 40, 60, 80]:
    t.circle(radius)
```

## Draw Tangent Circles with Two Alternating Colors
```python
# Import the required package
import turtle as t

# Basic settings of the pen
t.pensize(4)

# Draw tangent circles with two alternate colors, red and black
t.penup()
t.goto(-200,-50)
t.pendown()

color = "black"

for radius in [10, 20, 40, 60, 80]:
    if color == "black":
        color = "red"
    else:
        color = "black"
    t.pencolor(color)
    t.circle(radius)
```