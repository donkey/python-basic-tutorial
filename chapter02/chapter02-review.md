# Programming Fundamental -- Implemented in `Python`: Part 2, Review
# 程序设计基础 —— 以 `Python` 实现：第二部分，复习

In this part we will review the previously learned chapters,
include the following knowledge points:

* Basic types
* Arithmetic operations & logic operations
* Control flows
* Function definition and invocation

Although this is a review section, but we still learn a new skill, how to describe and solve the problems with flowcharts.

本部分我们将会复习之前学过的章节内容，包含如下知识点：

* 基础类型
* 算数运算与逻辑运算
* 控制流
* 函数的定义和调用

尽管这是复习章节，单我们仍然会学习新技能：如何通过过程图描述和解决问题。

## Tasks
## 本章任务

* Review the knowledge points of previous chapters
* Learn how to describe and solve the real world problems with flowcharts

* 复习之前章节的知识点
* 学习如何用流程图去描述和解决实际问题

## Review
## 复习

Let's review what we have learned via some `Python` code snippets.

让我们通过一些 `Python` 代码段一起复习下我们都学过了什么。

#### Basic Types of `Python`
#### Python 中的基本类型

There are **SIX** basic types in `Python`:

`Python` 中有六种基本类型：

- Number(数字): Integer, Float -> immutable (不可变)
- String(字符串): "abc", '100', """Long string""" -> immutable (不可变)
- Tuple(元组): (10, '100', 'abc') -> immutable (不可变)
- List(列表): [10, '100', 'abc'] -> mutable (可变的)
- Dictionay(字典): {1: 100, 'abc': '1x0'} -> key: value mutable (可变的)
- Set(集合): {10, 10, 10} -> {10} (不允许有重复元素) mutable (可变的)

Many times, you can use the syntax `<type>()` to convert from one type to another:

很多时候，你可以简单的通过 `<类型名>()` 的方式对不同类型实现转换：

```python
# Convertt between float and int
f = 10.231
i = int(f) # -> i = 10

i = 12
f = float(i) # -> f = 12.0

# Convert number to string
i = 12
s = str(i) # -> s = '12'

f = 12.23
s = str(f) # -> s = '12.23'

# list, tuple, set
tc = (1, 2, 3, 3)
lc = list(tc) # -> l = [1, 2, 3, 3]
sc = set(lc) # -> sc = {1, 2, 3}
tc = tuple(sc) # -> tc = (1, 2, 3)
lc = list(sc) # -> [1, 2, 3]
```

#### Arithmetic operations & Logic operations
#### 算数运算与逻辑运算

Usually, you can use `Python` as a calculator, it supports the basic operators of math:

通常来说，你可以把 `Python` 当作一个计算器，它支持基本的数学运算：

- add(加法): +
- subtract(减法): -
- multiply(乘法): *
- divide(除法): /
- power(幂): **
- floor divide(向下取整除法): //

```python
5 * (10 + 2) / 2 # -> 30
3 ** 2 # -> 9
3 ** 3 # -> 27
10 / 3 # -> 3.33333333333335
10 // 3 # -> 3
```

#### Control Flow
#### 控制流

Like other programming languages, `Python` also provide the three basic control flow syntax:

- Sequence
- Branch
- Loop

像其他编程语言一样， `Python` 也提供了三种基本控制流的语法：

- 顺序
- 分支
- 循环

```python
# loop
for i in [1, 2, 3]:
    print(i)
# Output:
# 1
# 2
# 3
i = 0
while i < 3:
    print(i)
    i = i + 1
# Output:
# 0
# 1
# 2

# branch
if 10 > 5:
    print("Larger")
else:
    print("Smaller")
# Output: Larger
```

#### Function Definition and Invocation
#### 函数的定义与调用

```python
# Syntax of function definition
# def <function name>(<args list>):
#     ...

def func(x, y):
    return x + y

# Call the function
print(func(10, 11))
# Output: 21

import turtle as t
# Invoke the function in a module
# <module>.<function>()
t.pendown()
```

## Flowcharts
## 流程图

In real world, before coding we usually arrange our logic, and proof the entire process, then we will start coding to implement it. What we do before coding is modeling, that means we should describe the problem and build the logic process to solve it. Commonly, the flowcharts is a good tool for this taks.

在现实世界中，开始编码之前，我们通常会梳理逻辑，并且证明整个处理流程，之后才编码实现。编码之前我们所做的事情叫做建模，就是描述问题并构建逻辑流程去解决问题。通常，对于这样的任务，流程图是个很好的工具。

> The following code snippet show this process, but it is not a flowchart,
> just a snippet of pseudo code, it is equivalent to a flowchart.

> 下面的代码段展示了这样一个过程，但是它不是流程图，
> 仅是一段伪代码，跟流程图是等价的。

```python
# Describe the logic process of your alogirthm
#
# 求解一个文件中的所有数字的累加总和
#
# 初始化 -> 声明一个列表变量：a_line_list
# 遍历文件，逐行读取文件并处理：(循环结构)
#    如果当前行是空的，跳过
#    如果当前行不是空的，用逗号切分并存入临时列表: alist
#    累加alist中的元素
#    将累加后的结果存入a_line_list
# 累加a_line_list里面的元素并输出

# Corresponding code implementation
def sum(alist):
    value = 0
    for item in alist:
        value = value + item
    return value

def sum_file(file_path):
    a_line_list = []
    for line in open(file_path):
        if not line.strip():
            continue
        # 10, 20, 30
        alist = line.split(',') # -> [10, 20, 30]
        line_sum = sum(alist)
        a_line_list.append(line_sum)

    value = sum(a_line_list)
    # print(value)
    return value

def sum_dir(dir_path):
    alist = []
    for file_path in ['1.csv', '2.csv', '3.csv']:
        file_result = sum_file(file_path)
        alist.append(file_result)
    return sum(alist)

print("Sum of all the files in this directory:", sum(alist))
```

### Execrise
### 练习

根据用户的正整数输入 `n` ，输出一个数列的求和结果 `x` ，数列形式如下：

1. x = 1 + 2 + 3 + 4 + 5 + ... + n
2. x = 1/1 + 1/2 + 1/3 + ... + 1/n

提示：

1. 尝试通过先描述流程，之后再编码的方式去解决问题
2. 使用循环结构，根据要求生成数列，并累加，可以通过 `range` 函数生成数列
3. 封装函数，讲用户输入作为函数的参数

## Appendix
## 附录

Install the `VS Code` as the new development environment of `Python`.

安装 `VS Code` 作为新的 `Python` 开发环境。
