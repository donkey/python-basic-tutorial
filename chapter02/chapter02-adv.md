# Programming Fundamental -- Implemented in `Python`: Part 2, Dive Into Python Syntax
# 程序设计基础 —— 以 `Python` 实现：第二部分，深入 Python 语法

In this chapter, I will introduce more syntaxs and show more characteristics of `Python`.
There are necessary for the later chapters.

本章中，我会介绍 `Python` 的更多语法并展示更多特性。这对与后续章节的学习是必要的。

## Tasks
## 本章任务

* `function`, `class`, `module`
* `list`, `dict`, and corresponding operations
* Basic IO

* 函数，类和模块
* `list`, `dict` 和对应的操作
* IO 基础


