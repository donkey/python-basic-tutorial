# Programming Fundamental-- Implemented in `Python`: Part 2, Definition and Invocation of Functions
# 程序设计基础 —— 以 `Python` 实现：第二部分，函数的定义与调用

In the previous chapter, I introduce the basic concepts and program structure about programming, and demonstrate the implementation by way of `Python` examples(I think you now also have a preliminary understanding of Python's syntax).

In this chapter, I will introduce a moew powerful abstraction "tool" in programming field: function, a set of expressions.

上一章中，我介绍了程序设计的基本概念和程序的基本结构，并且通过 `Python` 实现例子的方式做了演示（我想你现在应该也对 `Python` 的语法有了初步了解）。

本章中，我将介绍一个程序设计中更强大的抽象工具：函数，一组表达式的集合。

## Tasks
## 本章任务

- Understand the concepts of functions, learn how to define and invoke functions in `Python`.
- Demonstration with `Python` package `turtle`
- Complete the excerises

- 理解函数的概念，掌握 `Python` 中如何定义和调用函数
- 通过 `turtle` 实现基本的程序结构演示
- 完成课后练习

## Concept
## 概念




## Function Definition and Invocation in `Python`
## `Python` 中的函数定义和调用

Function is a important abstraction concept in programming field, almost all the different languages have implemented function with specific definition and invocation syntax, in this section, I will introduce the related syntax in `Python`, and lay it out piece by piece for you.

函数是程序设计的一个抽象概念，不同语言都有函数这种抽象的实现，具有特定的定义和调用的语法， 本章节将介绍 `Python` 中的相关语法，并逐步铺开详述。

### Definition
### 函数的定义

In `Python`, function can be defined by the following syntax:

```python
def <name>(<formal parameters>):
    return <return expression>
```

在 `Python` 中，函数以如下语法进行定义：

```python
def <函数名称>(<形式化参数>):
    return <返回值表达式>
```

## Excerises
## 课后练习

Reimplement the three excerises of previous chapter using functions.

使用函数的方式重新实现上一章节中的三个练习。
