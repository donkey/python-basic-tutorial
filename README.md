# python-basic-tutorial

Python basic tutorial

## Content

- [Chapter 01](./chapter01/chapter01.md): Python 简介
- [Chapter 02-01](./chapter02/chapter02-01.md): 程序设计基础 —— 以 `Python` 实现：第一部分，表达式，类型，变量和程序结构
- [Chapter 02-02](./chapter02/chapter02-02.md): 程序设计基础 —— 以 `Python` 实现：第二部分，函数的定义与调用
- [Chapter 02-03](./chapter02/chapter02-review.md): 程序设计基础 —— 以 `Python` 实现：第二部分，复习
- [Chapter 02-04](./chapter02/chapter02-adv.md): 程序设计基础 —— 以 `Python` 实现：第二部分，深入 Python 语法
